Team Members: Camillo Cosentino
	      Courtney Shek

1. Clone Git repo: git clone https://heskarioth@bitbucket.org/dsacommando/dockers_assessment.git
2. Git push to get the repository
3. Create Dockerfile.mysql following the parameters provided
4. Create Dockerfile.app following the parameters provided
5. Add, commit and push the new files
6. Create docker-compose.yaml
7. Run docker-compose up
8. Enjoy :) 
